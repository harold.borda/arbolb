﻿using ArbolB.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Bussiness.Controllers
{
    /// <summary>
    /// Controlador Bussiness
    /// </summary>
    public class ArbolBController : ControllerBase
    {
        #region Propiedades

        private readonly IArbolBRepository _arbolBRepository;

        #endregion

        #region Constructor

        /// <summary>
        /// Clase constructora.
        /// </summary>
        /// <param name="arbolBRepository"></param>
        public ArbolBController(IArbolBRepository arbolBRepository)
        {
            _arbolBRepository = arbolBRepository;
        }

        #endregion

        #region Metodos

        /// <summary>
        /// Realiza la creación de un arbol B
        /// </summary>
        /// <param name="nombreArbol"></param>
        /// <param name="nodos"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/ArbolB/CrearArbol/{nombreArbol}/{nodos}")]
        public dynamic CrearArbol(string nombreArbol, string nodos)
        {
            return _arbolBRepository.CrearArbol(nombreArbol, nodos);
        }

        /// <summary>
        /// Consulta el ancestro comun de dos nodos en un arbol
        /// </summary>
        /// <param name="nombreArbol"></param>
        /// <param name="nodo1"></param>
        /// <param name="nodo2"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("/api/ArbolB/LowestCommonAncestor/{nombreArbol}/{nodo1}/{nodo2}")]
        public dynamic LowestCommonAncestor(string nombreArbol, int nodo1, int nodo2)
        {
            return _arbolBRepository.LowestCommonAncestor(nombreArbol, nodo1, nodo2);
        }

        

        #endregion
    }
}
