using ArbolB;
using ArbolB.Repositories;
using Bussiness.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Net.Http;
using WireMock.Server;
using Xunit;

namespace ArbolB.Test
{
    public class UnitTest : IDisposable
    {
        private TestServer _server;
        public HttpClient Client { get; private set; }
        readonly FluentMockServer serviceMock;


        public UnitTest()
        {
            SetUpClient();
            serviceMock = FluentMockServer.Start();
        }

        private void SetUpClient()
        {
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            Client = _server.CreateClient();
        }
        

        
        [Fact]
        public void CrearArbolExitosamenteTest()
        {
            MemoryCache cache = new MemoryCache(new MemoryCacheOptions());
            ILogger<ArbolBRepository> logger = new Logger<ArbolBRepository>(new NullLoggerFactory());
            ArbolBRepository bussinessRepository = new ArbolBRepository(logger, cache);
            ArbolBController objControlador = new ArbolBController(bussinessRepository);
            string resultado = objControlador.CrearArbol("Arbol", "67,39,76,28,44,74,85,29,83,87");
            Assert.Equal("Arbol creado exitosamente", resultado);
        }

        [Fact]
        public void CrearArbolErrorTest()
        {
            MemoryCache cache = new MemoryCache(new MemoryCacheOptions());
            ILogger<ArbolBRepository> logger = new Logger<ArbolBRepository>(new NullLoggerFactory());
            ArbolBRepository bussinessRepository = new ArbolBRepository(logger, cache);
            ArbolBController objControlador = new ArbolBController(bussinessRepository);
            string resultado = objControlador.CrearArbol("Arbol", "Nodos");
            Assert.Equal("El arbol no se pudo crear", resultado);
        }

        [Fact]
        public void LowestCommonAncestor1Test()
        {
            MemoryCache cache = new MemoryCache(new MemoryCacheOptions());
            ILogger<ArbolBRepository> logger = new Logger<ArbolBRepository>(new NullLoggerFactory());
            ArbolBRepository bussinessRepository = new ArbolBRepository(logger, cache);
            ArbolBController objControlador = new ArbolBController(bussinessRepository);
            objControlador.CrearArbol("Arbol", "67,39,76,28,44,74,85,29,83,87");
            int resultado = objControlador.LowestCommonAncestor("Arbol", 29,44);
            Assert.Equal(39, resultado);
        }

        [Fact]
        public void LowestCommonAncestor2Test()
        {
            MemoryCache cache = new MemoryCache(new MemoryCacheOptions());
            ILogger<ArbolBRepository> logger = new Logger<ArbolBRepository>(new NullLoggerFactory());
            ArbolBRepository bussinessRepository = new ArbolBRepository(logger, cache);
            ArbolBController objControlador = new ArbolBController(bussinessRepository);
            objControlador.CrearArbol("Arbol", "67,39,76,28,44,74,85,29,83,87");
            int resultado = objControlador.LowestCommonAncestor("Arbol", 44, 85);
            Assert.Equal(67, resultado);
        }

        [Fact]
        public void LowestCommonAncestor3Test()
        {
            MemoryCache cache = new MemoryCache(new MemoryCacheOptions());
            ILogger<ArbolBRepository> logger = new Logger<ArbolBRepository>(new NullLoggerFactory());
            ArbolBRepository bussinessRepository = new ArbolBRepository(logger, cache);
            ArbolBController objControlador = new ArbolBController(bussinessRepository);
            objControlador.CrearArbol("Arbol", "67,39,76,28,44,74,85,29,83,87");
            int resultado = objControlador.LowestCommonAncestor("Arbol", 83, 87);
            Assert.Equal(85, resultado);
        }

        [Fact]
        public void LowestCommonAncestorNoExisteTest()
        {
            MemoryCache cache = new MemoryCache(new MemoryCacheOptions());
            ILogger<ArbolBRepository> logger = new Logger<ArbolBRepository>(new NullLoggerFactory());
            ArbolBRepository bussinessRepository = new ArbolBRepository(logger, cache);
            ArbolBController objControlador = new ArbolBController(bussinessRepository);
            int resultado = objControlador.LowestCommonAncestor("Arbol", 10, 20);
            Assert.Equal(0, resultado);
        }


        public void Dispose()
        {
            _server?.Dispose();
            Client?.Dispose();
            serviceMock.Stop();
        }
    }
}
