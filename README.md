La API puede ser puede consumir de dos maneras:

1) Ingresar a http://localhost:49700/swagger

En esta URL se podran ejecutar los dos métodos de la siguiente manera:

creararbol: se da click en try it our y se llenan los parametros
            nombreArbol : ejemplo -- arbol
            nodos; ejemplo -- 67,39,76,28,44,74,85,29,83,87
            
            Resultado esperado: 

            Exitoso: Mensaje -- "Arbol creado exitosamente"
            Error --   "El arbol no se pudo crear    
            
lowestcommonancestor: se da click en try it our y se llenan los parametros
            nombreArbol : ejemplo -- arbol
            nodo1 : ejemplo -- 29 
            nodo2 : ejemplo -- 44
            
            Resultado esperado: 

            Exitoso: ejemplo --- 39
            Error --   "El arbol no existe"
            

2) de maner alternativa se pueden ejecutar lo métodos de manera directa:

http://localhost:49700/api/arbolb/creararbol/arbol/67%2C39%2C76%2C28%2C44%2C74%2C85%2C29%2C83%2C87
http://localhost:49700/api/arbolb/lowestcommonancestor/arbol/29/44
