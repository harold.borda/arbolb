﻿using ArbolB.Api.objects;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;

namespace ArbolB.Repositories
{
    /// <summary>
    /// Respositorio 
    /// </summary>
    public class ArbolBRepository : IArbolBRepository
    {
        private readonly ILogger<ArbolBRepository> logger;
        private readonly IMemoryCache cache;

        /// <summary>
        /// Método Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="cache"></param>
        public ArbolBRepository(ILogger<ArbolBRepository> logger, IMemoryCache cache)
        {
            this.logger = logger;
            this.cache = cache;
        }


        #region Metodos

        /// <summary>
        /// Realiza la creación de un arbol B
        /// </summary>
        /// <param name="nombreArbol"></param>
        /// <param name="nodos"></param>
        /// <returns></returns>
        public string CrearArbol(string nombreArbol, string nodos)
        {
            try
            {
                CrearArbolClass objArbol = new CrearArbolClass();
                string[] data = nodos.Split(",");
                NodoClass raiz = new NodoClass();
                foreach (string item in data)
                {
                    raiz = objArbol.Insertar(Convert.ToInt16(item));
                }
                this.cache.Remove(nombreArbol + "-Raiz");
                this.cache.Set(nombreArbol + "-Raiz", raiz);

                return "Arbol creado exitosamente";
            }
            catch (FormatException ex)
            {
                logger.LogError("El arbol no se pudo crear: " + ex.ToString());
                return "El arbol no se pudo crear";
            }


        }

        /// <summary>
        /// Consulta el ancestro comun de dos nodos en un arbol
        /// </summary>
        /// <param name="nombreArbol"></param>
        /// <param name="nodo1"></param>
        /// <param name="nodo2"></param>
        /// <returns></returns>
        public dynamic LowestCommonAncestor(string nombreArbol, int nodo1, int nodo2)
        {
            NodoClass raiz = (NodoClass)this.cache.Get(nombreArbol + "-Raiz");
            LowestCommonAncestorClass objFunciones = new LowestCommonAncestorClass(raiz);
            if (objFunciones != null)
            {
                return objFunciones.LowestCommonAncestor(nodo1, nodo2);
            }
            else
            {
                return "El arbol no existe";
            }
        }


        #endregion Metodos


    }
}
