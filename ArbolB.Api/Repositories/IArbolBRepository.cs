﻿namespace ArbolB.Repositories
{
    /// <summary>
    /// Interface Bussiness
    /// </summary>
    public interface IArbolBRepository
    {
        /// <summary>
        /// Realiza la creación de un arbol B
        /// </summary>
        /// <param name="nombreArbol"></param>
        /// <param name="nodos"></param>
        /// <returns></returns>
        string CrearArbol(string nombreArbol, string nodos);

        /// <summary>
        /// Consulta el ancestro comun de dos nodos
        /// </summary>
        /// <param name="nombreArbol"></param>
        /// <param name="Nodo1"></param>
        /// <param name="nodo2"></param>
        /// <returns></returns>
        dynamic LowestCommonAncestor(string nombreArbol, int nodo1, int nodo2);
    }
}
