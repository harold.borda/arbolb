﻿using ArbolB.Api.objects;

namespace ArbolB.Api.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public static class ExisteFunction
    {
        /// <summary>
        /// Valida si un nodo ya existe en el arbol
        /// </summary>
        /// <param name="info"></param>
        /// <param name="raiz"></param>
        /// <returns></returns>
        public static bool Existe(int info, NodoClass raiz)
        {
            NodoClass reco = raiz;
            while (reco != null)
            {
                if (info == reco.info)
                {
                    return true;
                }
                else
                {
                    if (info > reco.info)
                    {
                        reco = reco.der;
                    }
                    else
                    {
                        reco = reco.izq;
                    }
                }
            }
            return false;
        }
    }
}
