﻿using System.Collections.Generic;

namespace ArbolB.Api.Helper
{
    /// <summary>
    /// Busca los nodos ancestro de un nodo
    /// </summary>
    public static class BuscarAncestrosFunction
    {
        /// <summary>
        /// Listado de los nodos ancestro
        /// </summary>
        public static readonly List<int> lstresultado = new List<int>();


        /// <summary>
        /// Busca los nodos ancestro de un nodo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static void BuscarAncestros(Dictionary<int, int> data, int valor)
        {
            if (data.TryGetValue(valor, out int ancestro))
            {
                if (ancestro != valor)
                {
                    lstresultado.Add(ancestro);
                    BuscarAncestros(data, ancestro);
                }
            }
        }
    }
}
