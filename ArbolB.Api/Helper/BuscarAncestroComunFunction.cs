﻿using System.Collections.Generic;

namespace ArbolB.Api.Helper
{
    /// <summary>
    /// Busca el ancestro comun dada la lista de ancestros de dos nodos
    /// </summary>
    public static class BuscarAncestroComunFunction
    {
        /// <summary>
        /// Busca el ancestro comun dada la lista de ancestros de dos nodos
        /// </summary>
        /// <param name="ancestros1"></param>
        /// <param name="ancestros2"></param>
        /// <returns></returns>
        public static int BuscarAncestroComun(List<int> ancestros1, List<int> ancestros2)
        {
            int econtrado = 0;
            foreach (int item in ancestros1)
            {
                econtrado = ancestros2.Find(x => x == item);
                if (econtrado != 0)
                {
                    return econtrado;
                }
            }
            return econtrado;
        }
    }
}
