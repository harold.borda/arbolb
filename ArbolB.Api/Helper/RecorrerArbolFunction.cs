﻿using ArbolB.Api.objects;
using System.Collections.Generic;

namespace ArbolB.Api.Helper
{
    /// <summary>
    /// Recorre todos los nodos del arbol
    /// </summary>
    public static class RecorrerArbolFunction
    {
        /// <summary>
        /// Almacena la informacion de un nodo y si ancestro
        /// </summary>
        public static Dictionary<int, int> dicNodos = new Dictionary<int, int>();
        /// <summary>
        /// Recorre todos los nodos del arbol
        /// </summary>
        /// <param name="reco"></param>
        /// <param name="nivel"></param>
        /// <param name="padre"></param>
        public static void RecorrerArbol(NodoClass reco, int nivel, NodoClass padre)
        {
            if (reco != null)
            {
                RecorrerArbol(reco.izq, nivel + 1, reco);
                dicNodos.Add(reco.info, padre.info);
                RecorrerArbol(reco.der, nivel + 1, reco);
            }
        }
    }
}
