﻿using ArbolB.Api.Helper;
using System.Collections.Generic;

namespace ArbolB.Api.objects
{
    /// <summary>
    /// Realiza la creación de un arbol B
    /// </summary>
    public class LowestCommonAncestorClass
    {
        /// <summary>
        /// Representa el nodo raiz del arbol
        /// </summary>
        private readonly NodoClass raiz;



        /// <summary>
        /// Clase Constructora
        /// </summary>
        /// <param name="data"></param>
        public LowestCommonAncestorClass(NodoClass data)
        {
            this.raiz = data;
        }


        /// <summary>
        /// Consulta el ancestro comun de dos nodos en un arbol
        /// </summary>
        /// <param name="nodo1"></param>
        /// <param name="nodo2"></param>
        /// <returns></returns>
        public int LowestCommonAncestor(int nodo1, int nodo2)
        {
            RecorrerArbolFunction.dicNodos.Clear();
            RecorrerArbolFunction.RecorrerArbol(raiz, 1, raiz);
            BuscarAncestrosFunction.lstresultado.Clear();
            BuscarAncestrosFunction.BuscarAncestros(RecorrerArbolFunction.dicNodos, nodo1);
            List<int> primerNodo = new List<int>(BuscarAncestrosFunction.lstresultado);
            BuscarAncestrosFunction.lstresultado.Clear();
            BuscarAncestrosFunction.BuscarAncestros(RecorrerArbolFunction.dicNodos, nodo2);
            List<int> segundorNodo = new List<int>(BuscarAncestrosFunction.lstresultado);
            BuscarAncestrosFunction.lstresultado.Clear();
            return BuscarAncestroComunFunction.BuscarAncestroComun(primerNodo, segundorNodo);
        }
    }
}
