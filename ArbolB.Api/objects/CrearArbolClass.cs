﻿using ArbolB.Api.Helper;

namespace ArbolB.Api.objects
{
    /// <summary>
    /// Crear un arbol  a partir de una serie de valores
    /// </summary>
    public class CrearArbolClass
    {
        /// <summary>
        /// Nodo Raiz del arbol
        /// </summary>
        private NodoClass raiz;

        /// <summary>
        /// Método constructor
        /// </summary>
        public CrearArbolClass()
        {
            raiz = null;
        }

        /// <summary>
        /// Crea in nodo en el arbol dado un valor
        /// </summary>
        /// <param name="info"></param>
        public NodoClass Insertar(int info)
        {
            if (!ExisteFunction.Existe(info, raiz))
            {
                NodoClass nuevo;
                nuevo = new NodoClass
                {
                    info = info,
                    izq = null,
                    der = null
                };
                if (raiz == null)
                    raiz = nuevo;
                else
                {
                    NodoClass anterior = null, reco;
                    reco = raiz;
                    while (reco != null)
                    {
                        anterior = reco;
                        if (info < reco.info)
                            reco = reco.izq;
                        else
                            reco = reco.der;
                    }
                    if (info < anterior.info)
                        anterior.izq = nuevo;
                    else
                        anterior.der = nuevo;
                }
            }
            return raiz;
        }
    }
}
