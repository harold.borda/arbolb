﻿namespace ArbolB.Api.objects
{
    /// <summary>
    /// Respresenta un nodo dentro del arbol
    /// </summary>
    public class NodoClass
    {
        /// <summary>
        /// Valor del Nodo
        /// </summary>
        public int info;
        /// <summary>
        /// Apuntadir al nodo Izquierdi
        /// </summary>
        public NodoClass izq;
        /// <summary>
        /// Apuntador al nodo derecho
        /// </summary>
        public NodoClass der;
    }
}
