﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using ArbolB.Repositories;

namespace ArbolB.Services
{
    /// <summary>
    /// Configuración de los repositorios.
    /// </summary>
    public static class SingletonService
    {
        /// <summary>
        /// Configuración de los repositorios.
        /// </summary>
        /// <param name="services"></param>
        public static void AddSingletonService(this IServiceCollection services)
        {
            services.AddSingleton<IArbolBRepository, ArbolBRepository>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
        }
    }
}
