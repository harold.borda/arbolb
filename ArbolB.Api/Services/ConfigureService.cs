﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ArbolB.Services
{
    /// <summary>
    /// Adicionar clases para el manejo de la configuración de la aplicación.
    /// </summary>
    public static class ConfigureService
    {
        /// <summary>
        /// Adicionar clases para el manejo de la configuración de la aplicación.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="Configuration"></param>
        public static void AddConfigureService(this IServiceCollection services,IConfiguration Configuration)
        {

        }
    }
}
